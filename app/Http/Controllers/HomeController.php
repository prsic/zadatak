<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Auth;


class HomeController extends Controller
{
    /**
     * Instantiate a new HomeController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    
    /**
     * Method to display home template for learning purpose
     * @return mixed blade template
     */
    public function showHome() 
    {
        $user = Auth::user();
        return view('home',['name' => $user->first_name]);
    }
    

}
