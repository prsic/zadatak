angular.module('todoResource',[])
.factory('Todo', function($resource) {
  return $resource('/api/todos/:id',{id: '@id'},{
    update: {
        method: 'PUT', 
        params: {id: '@id'}, 
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        
    }
  }); 
});