//********************************************
//json array employees contaning three objects
//********************************************
var employees = {
"employees":[
    {"firstName":"John", "lastName":"Doe"}, 
    {"firstName":"Anna",	"lastName":"Smith"},
    {"firstName":"Peter", "lastName":"Jones"}
]
};

console.log(employees);


//**********************
//js classes and objects
//**********************
var Person = function(first_name, last_name, gender){
    this.first_name = first_name;
    this.last_name = last_name;
    this.gender = gender;
    Person.somethingStatic = "something";
    
    Person.talkStatic = function(){
        console.log('Some static function');
    };
};

Person.prototype.sayHello = function() {
    console.log("Hello, I'm " + this.first_name);
};


var jelena = new Person('Jelena', 'Prsic', 'Female');
    jelena.sayHello();
    
Person.talkStatic();

//**************
//js inheritance
//**************
var Student = function(first_name, last_name, gender, subject){
    Person.call(this, first_name, last_name, gender);
    
    this.subject = subject;
};

Student.prototype.constructor = Student;
Student.prototype = Object.create(Person.prototype);

// Add a "sayGoodBye" method
Student.prototype.sayGoodBye = function(){
  console.log("Goodbye!");
};
// Replace the "sayHello" method
Student.prototype.sayHello = function(){
  console.log("Hello, I'm " + this.first_name + ". I'm studying "
              + this.subject + ".");
};

var jelenaStudent = new Student('Jelena', 'Prsic', 'Female', 'Programming');
jelenaStudent.sayHello();
jelenaStudent.sayGoodBye();
