var x = function () {
    var testVariable2 = 2;
    console.log("Test variable in anon: " + testVariable);
    console.log("Test variable 2 in anon: " + testVariable2)
    console.log("Test variable 3 in anon: " + testVariable3)
    return "i'm anonymous!"; 
};
var z = x();

console.log(z);

console.log("Test variable 1 outside anon in file: " + testVariable)
//console.log("Test variable 2 outside anon: " + testVariable2);
console.log("Test variable 3 outside anon: " + testVariable3);
console.log("Test variable 4 before app.js: " + testVariable4);
//console.log("Test variable 5 after app.js: " + testVariable5);


var todosApp = angular.module('todosApp', ['ngRoute', 'ngResource', 'todosCtrl', 'todoResource'], function($interpolateProvider){
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
}); 
