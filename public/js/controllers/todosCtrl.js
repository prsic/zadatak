angular.module('todosCtrl', [])

// inject the Todos service into our controller
.controller('todosController', function($scope, Todo) {
    
    $scope.required = true;
    
    // loading variable to show the spinning loading icon
    $scope.loading = true;

    // get all the todos first and bind it to the $scope.todos object
    // use the function we created in our service
    // GET ALL TODOS ==============
  
    var todos = Todo.query(function() {
        $scope.todos = todos;
        $scope.loading = false;
     }); //query() returns all the entries

    // function to handle submitting the form
    // SAVE A TODO ================

    $scope.saveTodo = function(){
        var todo = new Todo();
        todo.$save($scope.todo, function(){
            $scope.todos.push(todo);
        });
    }
    // MODIFY A TODO ================
    $scope.modifyTodo = function(data) {

        var todo = Todo.get({id: data.id});
        todo.$update(data);
        
    };
    // function to handle deleting a todo
    // DELETE A TODO ====================================================
    $scope.deleteTodo = function(todo) {
        Todo.delete({id: todo.id});
        var index = $scope.todos.indexOf(todo);
        $scope.todos.splice(index,1);
    
    };

})
.directive( 'editInPlace', function() {
   //@TODO change blur and active to angular
  return {
    restrict: 'E',
    scope: { ngModel: '=',
             type: '@'
           },
    template: '<span ng-click="edit()" ng-bind="ngModel"></span><input type="<%type%>" ng-model="ngModel" ng-blur="validateInput()" ng-required="required"></input>',
    link: function ( $scope, element, attrs ) { 
     var initial = $scope.ngModel;
      // Let's get a reference to the input element, as we'll want to reference it.
      var inputElement = angular.element( element.children()[1] );
      
      // This directive should have a set class so we can style it.
      element.addClass( 'edit-in-place' );
      
      // Initially, we're not editing.
      $scope.editing = false;
      
      // ng-click handler to activate edit-in-place
      $scope.edit = function () {
            $scope.editing = true;
        
            // We control display through a class on the directive itself. See the CSS.
            element.addClass( 'active' );

            // And we must focus the element. 
            // `angular.element()` provides a chainable array, like jQuery so to access a native DOM function, 
            // we have to reference the first element in the array.
            inputElement[0].focus();
      };
      $scope.validateInput = function (){
          $scope.editing = false;
          element.removeClass( 'active' );
          
          if(inputElement.val()==="") $scope.ngModel = initial;
      }
    }
  };
})


