@extends('layouts.master')

@section('title', 'Register')
@section('content')
<div class="panel panel-default">
    <div class="panel-body">
        <form role="form" method="POST" action="register">
            {!! csrf_field() !!}
            <div>
                Email
                <input type="email" name="email" value="{{ old('email') }}" class="form-control input-sm">
            </div>

            <div>
                Password
                <input type="password" name="password" class="form-control input-sm">
            </div>

            <div>
                Confirm Password
                <input type="password" name="password_confirmation" class="form-control input-sm">
            </div>
            <div>
                First Name
                <input type="text" name="first_name" value="{{ old('first_name') }}" class="form-control input-sm">
            </div>
            <div>
                Last Name
                <input type="text" name="last_name" value="{{ old('last_name') }}" class="form-control input-sm">
            </div>
            <div>
                Company
                <input type="text" name="company" value="{{ old('company') }}" class="form-control input-sm">
            </div>
            <div>
                Country
                {!! Form::select('country_id', $countries) !!}
            </div>

            <div>
                <input type="submit" value="Register" class="btn btn-info btn-block">
            </div>
        </form>
    </div>
</div>
@stop