@extends('layouts.master')
@section('title','Login')
@section('content')
<div class="panel panel-default">
      @if (Auth::guest())
      <div class="panel-heading">
          <h3 class="panel-title"><a href="{{ url('register') }}">Please sign up <small>It's free!</small></a></h3>
      </div>
      @endif
      <div class="panel-body">
         {!! Form::open(array('url'=>'auth/login')) !!}
          {!! csrf_field() !!}
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                       {!! Form::email('email', null, array('class'=>'form-control input-sm','placeholder'=>'Email Address')) !!}
                      
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                      {!! Form::password('password', array('class'=>'form-control input-sm','placeholder'=>'Password')) !!}
                    </div>
                </div>
            </div>

          {!! Form::submit('Login', array('class'=>'btn btn-info btn-block')) !!}

        {!! Form::close() !!}
      </div>
</div>
@stop