@extends('layouts.master')

@section('title', 'This is Title')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    <!-- NEW TODO FORM =============================================== -->
    <form ng-submit="saveTodo()" class="col-lg-4 col-lg-offset-4" > <!-- ng-submit will disable the default form action and use our function -->
    
        <!-- TITLE -->
        <div class="form-group">
            <input type="text" class="form-control input-lg" name="name" ng-model="todo.name" placeholder="Name">
        </div>
        
        <!-- DESCRIPTION -->
        <div class="form-group">
            <input type="text" class="form-control input-group-lg" name="description" ng-model="todo.description" placeholder="Description">
        </div>
        
        <!-- DESCRIPTION -->
        <div class="form-group">
            <input type="number" class="form-control input-group-lg" name="priority" ng-model="todo.priority" placeholder="Priority" min="1">
        </div>
        
        <!-- SUBMIT BUTTON -->
        <div class="form-group text-right">   
            <button type="submit" class="btn btn-primary btn-lg">Submit</button>
        </div>
    </form>
    
    <!-- LOADING ICON =============================================== -->
    <!-- show loading icon if the loading variable is set to true -->
    <p class="text-center" ng-show="loading"><span class="fa fa-meh-o fa-5x fa-spin"></span></p>
    
    <!-- THE TODOS =============================================== -->
    <!-- hide these todos if the loading variable is true -->
    <div>
        <table class="table table-hover" >
             <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Priority</th>
                <th>Done?</th>
                <th></th>
                <th></th>
              </tr>
              <tr ng-hide="loading" ng-class="{'text-muted': todo.done, 'danger': !todo.done && todo.priority<=2}" ng-repeat="todo in todos | orderBy: ['done', 'priority']">
                  <td><edit-in-place  type="text" ng-model="todo.name"></edit-in-place></td>
                  <td><edit-in-place  type="text" ng-model="todo.description"></edit-in-place></td> 
                  <td><edit-in-place  type="number" ng-model="todo.priority"></edit-in-place></td>
                  <td><span ng-if="!todo.done"><a href="#" ng-click="todo.done = 1; modifyTodo(todo)" title="Mark as done!"><i class="glyphicon glyphicon-ok" ></i></a></span></td>
                  <td><a href="#" ng-click="modifyTodo(todo)" ><i class="glyphicon glyphicon-edit"></i></a></td>
                  <td><a href="#" ng-click="deleteTodo(todo)" title="Remove"><i class="glyphicon glyphicon-remove"></i></a></td>
                  
              </tr>
        </table>
       
    </div>
    
@endsection