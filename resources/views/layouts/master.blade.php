<html>
    <head>
        <title>My App - @yield('title')</title>
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet">
        <style>
            .edit-in-place span {
                cursor: pointer;
              }

              .edit-in-place input {
                display: none;
              }

              .edit-in-place.active span {
                display: none;
              }

              .edit-in-place.active input {
                display: inline-block;
              }
        </style>
    </head>
    
    <body ng-app="todosApp" ng-controller="todosController">
        @include('partials.header')
        
        
        <div class="container">
            <div class='row'>
                <div class='col-sm-12'>
                    @include('errors.errors')
            
                    @yield('content')

                </div>
                
            </div>    
            
        </div>
        
        @include('partials.footer')
    </body>
</html>