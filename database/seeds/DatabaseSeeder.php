<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        //Seed the users
        $this->call('UserTableSeeder');

        //Seed the countries
        $this->call('CountriesSeeder');
        $this->command->info('Seeded the countries!'); 
        
        //Seed the TODO items
        $this->call('TodoItemTableSeeder');
        $this->command->info('Seeded the TODO items!'); 
    }
}
