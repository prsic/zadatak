<?php

use Illuminate\Database\Seeder;
use App\TodoItem;

class TodoItemTableSeeder extends Seeder {
    public function run()
    {
        DB::table('todo_items')->delete();
        TodoItem::create(array(
            'name'     => 'TODO item 1',
            'description' => 'This is just a test',
            'priority'    => 1,
            'done' => false,
            'user_id' => 1,
            
        ));
        TodoItem::create(array(
            'name'     => 'TODO item 2',
            'description' => '',
            'priority'    => 1,
            'done' => false,
            'user_id' => 1,
            
        ));
        TodoItem::create(array(
            'name'     => 'TODO item 3',
            'description' => 'This is just a test of item 3 something longer.',
            'priority'    => 2,
            'done' => false,
            'user_id' => 1,
            
        ));
    }
}
