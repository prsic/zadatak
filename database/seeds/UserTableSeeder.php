<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{

public function run()
{
    DB::table('users')->delete();
    User::create(array(
        'first_name'     => 'Jelena',
        'last_name' => 'Prsic',
        'email'    => 'jelena.p@vivify.com',
        'company' => 'vivify ideas',
        'country_id' => '1',
        'password' => Hash::make('jelena'),
    ));
}

}